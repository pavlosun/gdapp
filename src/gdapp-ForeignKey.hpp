#pragma once

#include <cassert>
#include <libgda/libgda.h>
#include <string>
#include <iostream>

namespace gdapp {

class BaseForeignKey {
  public:
    virtual GdaDbFkey *gobj() = 0;
    virtual ~BaseForeignKey() = default;
};

enum class FkeyActions {ON_DELETE, ON_UPDATE};
enum class KeyReferenceActions { NO_ACTION, SET_NULL, RESTRICT, SET_DEFAULT, CASCADE };

template<FkeyActions T>
class FkeyPolicy {
    KeyReferenceActions _action;
    public:
	explicit FkeyPolicy(KeyReferenceActions action) : _action(action) {}
	KeyReferenceActions get_action() const {return _action;}
};

using OnDelete = FkeyPolicy<FkeyActions::ON_DELETE>;
using OnUpdate = FkeyPolicy<FkeyActions::ON_UPDATE>;

class ForeignKey : public BaseForeignKey {
    GdaDbFkey *_fkey = nullptr;

    template <class... Ts>
	ForeignKey(OnDelete key, Ts... args) : ForeignKey(args...) {
	assert(_fkey);
        switch (key.get_action()) {
        case KeyReferenceActions::NO_ACTION:
            gda_db_fkey_set_ondelete(_fkey, GDA_DB_FKEY_NO_ACTION);
	    break;
        case KeyReferenceActions::CASCADE:
            gda_db_fkey_set_ondelete(_fkey, GDA_DB_FKEY_CASCADE);
	    break;
        case KeyReferenceActions::RESTRICT:
            gda_db_fkey_set_ondelete(_fkey, GDA_DB_FKEY_RESTRICT);
	    break;
        case KeyReferenceActions::SET_DEFAULT:
            gda_db_fkey_set_ondelete(_fkey, GDA_DB_FKEY_SET_DEFAULT);
	    break;
        case KeyReferenceActions::SET_NULL:
            gda_db_fkey_set_ondelete(_fkey, GDA_DB_FKEY_SET_NULL);
	    break;
        }
    }

    template <class... Ts>
	ForeignKey(OnUpdate key, Ts... args) : ForeignKey(args...) {
	assert(_fkey);
        switch (key.get_action()) {
        case KeyReferenceActions::NO_ACTION:
            gda_db_fkey_set_onupdate(_fkey, GDA_DB_FKEY_NO_ACTION);
        case KeyReferenceActions::CASCADE:
            gda_db_fkey_set_onupdate(_fkey, GDA_DB_FKEY_CASCADE);
        case KeyReferenceActions::RESTRICT:
            gda_db_fkey_set_onupdate(_fkey, GDA_DB_FKEY_RESTRICT);
        case KeyReferenceActions::SET_DEFAULT:
            gda_db_fkey_set_onupdate(_fkey, GDA_DB_FKEY_SET_DEFAULT);
        case KeyReferenceActions::SET_NULL:
            gda_db_fkey_set_onupdate(_fkey, GDA_DB_FKEY_SET_NULL);
        }
    }

    ForeignKey() {
	assert(!_fkey);
	_fkey = gda_db_fkey_new();
    }

    public:

    template <class ...Ts>
    ForeignKey(std::string refTable, std::string field, std::string reffield, Ts...args) : ForeignKey(args...) {
	assert(_fkey);

	gda_db_fkey_set_ref_table(_fkey, refTable.c_str());
	gda_db_fkey_set_field (_fkey, field.c_str(), reffield.c_str());
    }

    virtual ~ForeignKey() {
	g_object_unref(_fkey);
    }

    GdaDbFkey *gobj() override {
	return _fkey;
    }
};

}
