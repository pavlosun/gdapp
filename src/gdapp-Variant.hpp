#pragma once

#include <cstddef>
#include <glibmm.h>
#include <libgda.h>
#include <string>

namespace gdapp {

class TypeException final : public std::exception {
    std::string mMessage;

  public:
    TypeException(std::string msg, const Glib::ustring &file, int line) : mMessage(std::move(msg)) {
        mMessage = file + ":" + std::to_string(line) + " " + msg;
    }

    [[nodiscard]] auto what() const noexcept -> const char * override { return mMessage.c_str(); }
};

using intPtr = std::shared_ptr<int>;
using stringPtr = std::shared_ptr<std::string>;
using datetimePtr = std::shared_ptr<Glib::DateTime>;
using doublePtr = std::shared_ptr<double>;

class Variant final {
    GValue *_gobject{nullptr};

  public:
    Variant() = default;

    Variant(const Variant &src) { _gobject = gda_value_copy(src._gobject); }

    auto operator=(const Variant &src) -> Variant & {
        if (this == &src)
            return *this;

        gda_value_free(_gobject);
        _gobject = gda_value_copy(src._gobject);

        return *this;
    }

    Variant(Variant &&src) noexcept {
        _gobject = src._gobject;
        src._gobject = nullptr;
    }

    auto operator=(Variant &&src) noexcept -> Variant & {
        if (this == &src)
            return *this;

        gda_value_free(_gobject);
        _gobject = src._gobject;
        src._gobject = nullptr;

        return *this;
    }

    ~Variant() { gda_value_free(_gobject); }

    explicit Variant(GValue *src) { _gobject = src; }

    explicit Variant(const GValue &src) { _gobject = gda_value_copy(&src); }

    [[nodiscard]] inline static auto create(const GValue *src) -> Variant {
	GValue *val = gda_value_copy(src);
	return Variant{val};
    }

    Variant(int val) {
        _gobject = gda_value_new(G_TYPE_INT);
        g_value_set_int(_gobject, val);
    }

    Variant(char val) {
        _gobject = gda_value_new(G_TYPE_CHAR);
        g_value_set_schar(_gobject, val);
    }

    Variant(unsigned int val) {
        _gobject = gda_value_new(G_TYPE_UINT);
        g_value_set_uint(_gobject, val);
    }

    Variant(unsigned char val) {
        _gobject = gda_value_new(G_TYPE_UCHAR);
        g_value_set_uchar(_gobject, val);
    }

    Variant(double val) {
        _gobject = gda_value_new(G_TYPE_DOUBLE);
        g_value_set_double(_gobject, val);
    }

    Variant(float val) {
        _gobject = gda_value_new(G_TYPE_FLOAT);
        g_value_set_float(_gobject, val);
    }

    Variant(long val) {
        _gobject = gda_value_new(G_TYPE_LONG);
        g_value_set_long(_gobject, val);
    }

    Variant(unsigned long val) {
        _gobject = gda_value_new(G_TYPE_ULONG);
        g_value_set_ulong(_gobject, val);
    }

    Variant([[maybe_unused]]decltype(nullptr) val) {
        _gobject = gda_value_new(GDA_TYPE_NULL);
    }

    Variant(const std::string &val) {
        _gobject = gda_value_new(G_TYPE_STRING);
        g_value_set_string(_gobject, val.c_str());
    }

    Variant(const Glib::DateTime &val) {
        _gobject = gda_value_new(G_TYPE_DATE_TIME);
        auto dt = val.gobj_copy();
        g_value_take_boxed(_gobject, dt);
    }

    Variant(Glib::DateTime &&val) {
        _gobject = gda_value_new(G_TYPE_DATE_TIME);
        auto dt = val.gobj_copy();
        g_value_take_boxed(_gobject, dt);
    }

    [[nodiscard]] auto getInt() const -> int {

	if (isSQLNULL() || !G_VALUE_HOLDS_INT(_gobject))
	    throw gdapp::TypeException("The requested type is different from the storred type", __FILE__, __LINE__);

	return g_value_get_int(_gobject);
    }

    [[nodiscard]] auto getChar() const -> char {

	if (isSQLNULL() || !G_VALUE_HOLDS_CHAR(_gobject))
	    throw gdapp::TypeException("The requested type is different from the storred type", __FILE__, __LINE__);

	return g_value_get_schar(_gobject);
    }

    [[nodiscard]] auto getUChar() const -> unsigned char {

	if (isSQLNULL() || !G_VALUE_HOLDS_UCHAR(_gobject))
	    throw gdapp::TypeException("The requested type is different from the storred type", __FILE__, __LINE__);

	return g_value_get_uchar(_gobject);
    }

    [[nodiscard]] auto getUInt() const -> uint {

	if (isSQLNULL() || !G_VALUE_HOLDS_UINT(_gobject))
	    throw gdapp::TypeException("The requested type is different from the storred type", __FILE__, __LINE__);

	return g_value_get_uint(_gobject);
    }

    [[nodiscard]] auto getDouble() const -> double {

	if (isSQLNULL() || !G_VALUE_HOLDS_DOUBLE(_gobject))
	    throw gdapp::TypeException("The requested type is different from the storred type", __FILE__, __LINE__);

	return g_value_get_double(_gobject);
    }

    [[nodiscard]] auto getFloat() const -> float {

	if (isSQLNULL() || !G_VALUE_HOLDS_FLOAT(_gobject))
	    throw gdapp::TypeException("The requested type is different from the storred type", __FILE__, __LINE__);

	return g_value_get_float(_gobject);
    }

    [[nodiscard]] auto getLong() const -> long{

	if (isSQLNULL() || !G_VALUE_HOLDS_LONG(_gobject))
	    throw gdapp::TypeException("The requested type is different from the storred type", __FILE__, __LINE__);

	return g_value_get_long(_gobject);
    }

    [[nodiscard]] auto getULong() const -> unsigned long {

	if (isSQLNULL() || !G_VALUE_HOLDS_ULONG(_gobject))
	    throw gdapp::TypeException("The requested type is different from the storred type", __FILE__, __LINE__);

	return g_value_get_ulong(_gobject);
    }

    [[nodiscard]] auto getString() const -> std::string {

	if (isSQLNULL() || !G_VALUE_HOLDS_STRING(_gobject))
	    throw gdapp::TypeException("The requested type is different from the storred type", __FILE__, __LINE__);

	return g_value_get_string(_gobject);
    }

    [[nodiscard]] auto getDateTime() const -> Glib::DateTime {

	if (isSQLNULL() || !G_VALUE_HOLDS_BOXED (_gobject))
	    throw gdapp::TypeException("The requested type is different from the storred type", __FILE__, __LINE__);

	return Glib::DateTime{static_cast<GDateTime *>(g_value_get_boxed(_gobject)), true};
    }

    [[nodiscard]] auto isSQLNULL() const -> bool { return GDA_VALUE_HOLDS_NULL(_gobject) ? true : false; }

    auto gobjCopy() -> GValue * { return gda_value_copy(_gobject); }
};

} // namespace gdapp
