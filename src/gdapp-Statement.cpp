
#include "gdapp-Statement.hpp"

namespace gdapp {

SQLStatement::SQLStatement(std::shared_ptr<gdapp::Connection> cnc) : _stmt{nullptr}, _pset{nullptr}, ptrcnc{cnc} {}

auto SQLStatement::bind(const std::string &id, gdapp::Variant val) -> Statement & {
    GError *error = nullptr;

    GdaHolder *hd = gda_set_get_holder(_pset.get(), id.c_str());

    if (!hd)
        throw gdapp::SQLexception("Wrong holder name", __FILE__, __LINE__);

    if (!gda_holder_take_value(hd, val.gobjCopy(), &error))
        throw Glib::Error(error, true);

    return *this;
}

auto SQLStatement::bind(const std::string &id, [[maybe_unused]] std::nullptr_t val) -> Statement & {

    GError *error = nullptr;

    GdaHolder *hd = gda_set_get_holder(_pset.get(), id.c_str());

    if (!hd)
        throw gdapp::SQLexception("Wrong holder name", __FILE__, __LINE__);

    GValue *value = gda_value_new(GDA_TYPE_NULL);
    gda_value_set_null(value);

    if (!gda_holder_take_value(hd, value, &error))
        throw Glib::Error(error, true);

    return *this;
}

auto SQLStatement::prepare(const std::string &sql) -> Statement & {
    GError *error = nullptr;
    GdaSet *_set = nullptr;

    if (!ptrcnc->isOpen())
        throw SQLexception("Connection must be open before using in Statement", __FILE__, __LINE__);

    _stmt = GPtr<GdaStatement>(gda_connection_parse_sql_string(ptrcnc->gobj().get(), sql.c_str(), &_set, &error));

    if (!_stmt)
        throw Glib::Error{error, true};

    if (_pset)
        _pset.reset(_set);
    else
        _pset = GPtr<GdaSet>(_set);

    return *this;
}

void SQLStatement::execute() {
    GdaSet *_lrow = nullptr;
    GError *error = nullptr;

    gint res =
        gda_connection_statement_execute_non_select(ptrcnc->gobj().get(), _stmt.get(), _pset.get(), &_lrow, &error);

    if (res == -1 && error)
        throw Glib::Error(error, true);

    int pos{0};
    GdaHolder *holder{nullptr};
    std::string name;

    if (_lrow) {
        if (!_last_row.empty())
            _last_row.clear();

        while ((holder = gda_set_get_nth_holder(_lrow, pos++)) != nullptr) {
            gchar *name_p{nullptr};
            g_object_get(holder, "name", &name_p, nullptr);
            name = name_p;
            g_free(name_p);

            auto gval = gda_holder_get_value(holder);
            _last_row[name] = gdapp::Variant{*gval};
        }
    }
}

auto SQLStatement::query() -> std::unique_ptr<gdapp::DataModel> {
    GError *error = nullptr;

    GdaDataModel *res = gda_connection_statement_execute_select(ptrcnc->gobj().get(), _stmt.get(), _pset.get(), &error);

    if (!res)
        throw Glib::Error(error, true);

    return std::make_unique<gdapp::SQLDataModel>(res);
}

auto SQLStatement::last_row() const -> const std::map<std::string, gdapp::Variant> & { return _last_row; }

} // namespace gdapp
