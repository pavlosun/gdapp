#pragma once

#include "gdapp-Connection.hpp"
#include "gdapp-Column.hpp"
#include "gdapp-ForeignKey.hpp"
#include <glibmm.h>
#include <string>
#include <libgda/libgda.h>
#include <iostream>
#include <cstring>
#include <typeinfo>

namespace gdapp {


class DdlModifiable {
    public:
	virtual void create(gdapp::Connection &cnc) = 0;
	virtual void drop(gdapp::Connection &cnc) = 0;
	virtual void rename(gdapp::Connection &cnc) = 0;
	virtual ~DdlModifiable() = default;
};

class BaseTable : public DdlModifiable {
    GdaDbTable *_table = nullptr;

  public:
    void add(BaseColumn &column) {
	gda_db_table_append_column(_table, column.gobj());
    }

    void add(ForeignKey &fkey) {
	gda_db_table_append_fkey(_table, fkey.gobj());
    }

    void set_name(const char *name) {
	gda_db_base_set_name(GDA_DB_BASE(_table), name);
    }

    void set_name(const std::string &name) {
	gda_db_base_set_name(GDA_DB_BASE(_table), name.c_str());
    }

    const char *get_name() const {
	return gda_db_base_get_name(GDA_DB_BASE(_table));
    }

    BaseTable() { _table = gda_db_table_new(); }

    void create(gdapp::Connection &cnc) override {
	GError *error = nullptr;
	if (!gda_ddl_modifiable_create (GDA_DDL_MODIFIABLE (_table), cnc.gobj().get(), NULL, &error))
	    throw Glib::Error(error);

    }

    void drop(gdapp::Connection &cnc) override {
	GError *error = nullptr;

	if (!gda_ddl_modifiable_drop (GDA_DDL_MODIFIABLE (_table), cnc.gobj().get(), NULL, &error))
	    throw Glib::Error(error);
    }

    void rename(gdapp::Connection &cnc) override {
	GError *error = nullptr;

	if (!gda_ddl_modifiable_drop (GDA_DDL_MODIFIABLE (_table), cnc.gobj().get(), NULL, &error))
	    throw Glib::Error(error);
    }

    virtual ~BaseTable() { g_object_unref(_table); }
};

}
