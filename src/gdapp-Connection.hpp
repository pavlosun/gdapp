#pragma once

#include "gdapp-Types.hpp"
#include "gdapp-Exception.hpp"
#include <libgda/libgda.h>
#include <sigc++/sigc++.h>
#include <string>
#include <map>

namespace gdapp {

/**
 * Interface for all providesrs
 *
 */
class Provider {
    public:
	virtual const std::string &name() const = 0;
	virtual const std::string &get_cnc_string() const  = 0;
	virtual const std::string &get_auth_string() const  = 0;

	virtual ~Provider() = default;
};

class SQLiteProvider : public Provider {
    const std::string mName{"SQLite"};
    const std::string _auth_string;
    const std::string _cnc_string;

  public:
    SQLiteProvider(const std::string &cnc_string, std::string auth_string = std::string())
        : _auth_string(std::move(auth_string)), _cnc_string{mName + "://" + cnc_string} {}

    const std::string &name() const override { return mName; }
    const std::string &get_cnc_string() const override { return _cnc_string; }
    const std::string &get_auth_string() const override { return _auth_string; }
};

class MySQLProvider : public Provider {
    const std::string mName{"MySQL"};
    const std::string _auth_string;
    const std::string _cnc_string;

  public:
    MySQLProvider(const std::string &cnc_string, std::string auth_string = std::string())
        : _auth_string(std::move(auth_string)), _cnc_string{mName + "://" + cnc_string} {}

    const std::string &name() const override { return mName; }
    const std::string &get_cnc_string() const override { return _cnc_string; }
    const std::string &get_auth_string() const override { return _auth_string; }
};

class PostgreSQLProvider : public Provider {
    const std::string mName{"PostgreSQL"};
    const std::string _auth_string;
    const std::string _cnc_string;

  public:
    PostgreSQLProvider(const std::string &cnc_string, std::string auth_string = std::string())
        : _auth_string(std::move(auth_string)), _cnc_string{mName + "://" + cnc_string} {}

    const std::string &name() const override { return mName; }
    const std::string &get_cnc_string() const override { return _cnc_string; }
    const std::string &get_auth_string() const override { return _auth_string; }
};

class Connection {
  public:
    using OpenCloseSignal = sigc::signal<void>;
    OpenCloseSignal _open_signal;
    OpenCloseSignal _close_signal;

  public:
    virtual void open() = 0;
    virtual void close() = 0;
    [[nodiscard]] virtual auto isOpen() const -> bool = 0;
    virtual GSharedPtr<GdaConnection> gobj() = 0;

    Connection() = default;
    virtual ~Connection() = default;
    Connection(const Connection &) = default;
    Connection(Connection &&) noexcept = default;
    auto operator=(const Connection &) -> Connection & = default;
    auto operator=(Connection &&) noexcept -> Connection & = default;

    OpenCloseSignal signal_open() { return _open_signal; }
    OpenCloseSignal signal_close() { return _close_signal; }
};

class SQLConnection final : public Connection {
    GSharedPtr<GdaConnection> _pcnc;
    gulong connection_id;

    //static void on_connection_open(Connection *user_data);
    //static void on_connection_close(Connection *user_data);
    static void on_connection_status_changed(GdaConnection *cnc, GdaConnectionStatus status,
                                             gpointer user_data);

  public:
    void open() override;
    void close() override;
    [[nodiscard]] auto isOpen() const -> bool override;
    GSharedPtr<GdaConnection> gobj() override;

    SQLConnection(const Provider &provider);
    ~SQLConnection();
};

} // namespace gdapp
