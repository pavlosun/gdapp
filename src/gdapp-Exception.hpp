
#pragma once

#include <exception>
#include <string>

namespace gdapp {

class SQLexception final : public std::exception {
    std::string mMessage;

  public:
    SQLexception(std::string msg, const std::string &file, int line) : mMessage(std::move(msg)) {
        mMessage = file + ":" + std::to_string(line) + " " + msg;
    }

    [[nodiscard]] auto what() const noexcept -> const char * override { return mMessage.c_str(); }
};

} // namespace gdapp
