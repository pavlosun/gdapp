#include <gtest/gtest.h>
#include "../gdapp-Variant.hpp"

TEST(Variant, DefaultCtor)
{
    int value = 3;
    gdapp::Variant var;
    gdapp::Variant var2(value);

    var = var2;

    EXPECT_EQ(var.getInt(), value);
}

TEST(Variant, CheckMoveConstructor)
{
    int value = 3;
    gdapp::Variant var{value};
    gdapp::Variant var2;

    var2 = std::move(var);
    EXPECT_EQ(var2.getInt(), value);
}

TEST(Variant, CopyConstructor)
{
    int value = 3;
    gdapp::Variant var{value};
    gdapp::Variant var2 = var;

    EXPECT_EQ(var2.getInt(), var.getInt());
}

TEST(Variant,CtorIntFromVariable)
{
    int value = -3;
    gdapp::Variant var(value);

    EXPECT_EQ(var.getInt(), value);
}

TEST(Variant,CtorIntFromValue)
{
    gdapp::Variant var(-3);

    EXPECT_EQ(var.getInt(), -3);
}

TEST(Variant, CtorUnsignedIntFromVariable)
{
    unsigned int value = 3;
    gdapp::Variant var(value);

    EXPECT_EQ(var.getUInt(), value);
}

TEST(Variant, CtorUnsignedIntFromValue)
{
/* We have to explicitely tell that we have unsigned int. Otherwise int constructor will be called */
    gdapp::Variant var(3u);

    EXPECT_EQ(var.getUInt(), 3u);
}

TEST(Variant, CtorSignedCharFromVariable)
{
    char value = 'C';
    gdapp::Variant var(value);

    EXPECT_EQ(var.getChar(), value);
}

TEST(Variant, CtorUnsignedCharFromVariable)
{
    unsigned char value = -3;
    gdapp::Variant var(value);

    EXPECT_EQ(var.getUChar(), value);
}

TEST(Variant, CtorSignedLongFromVariable)
{
    long value = -3;
    gdapp::Variant var(value);

    EXPECT_EQ(var.getLong(), value);
}

TEST(Variant, CtorUnsignedLongFromVariable)
{
    unsigned long value = -3;
    gdapp::Variant var(value);

    EXPECT_EQ(var.getULong(), value);
}

TEST(Variant, CtorDoubleFromValue)
{
    gdapp::Variant var(3.14);

    EXPECT_EQ(var.getDouble(), 3.14);
}

TEST(Variant, CtorBlobFromValue)
{
    gdapp::Variant var(3.14);

    EXPECT_EQ(var.getDouble(), 3.14);
}


TEST(Variant, CtorSQLNULL)
{
    gdapp::Variant var(nullptr);

    EXPECT_TRUE(var.isSQLNULL());
}

TEST(Variant, CtorGetWrongType)
{
    gdapp::Variant var(3);

    char res = 'c';

    EXPECT_THROW({
	    res = var.getChar();
	    }, gdapp::TypeException);

    EXPECT_EQ(res, 'c'); // Mostly to avoid unused variable warning from the compiler
}

int main(int argc, char *argv[]) {
    testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}


