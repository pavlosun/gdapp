#include <gmock/gmock.h>
#include "../gdapp-Connection.hpp"
#include "../gdapp-Statement.hpp"
#include "glibmm/error.h"
#include <memory>

class ConnectionMock : public gdapp::Connection {
  public:
    MOCK_METHOD(void, open, (), (override));
    MOCK_METHOD(void, close, (), (override));
    MOCK_METHOD(bool, isOpen, (), (const, override));
    MOCK_METHOD(gdapp::GSharedPtr<GdaConnection>, gobj, (), (override));
};

class StatementTest : public ::testing::Test {
    public:
	std::shared_ptr<ConnectionMock> cnc;
	std::shared_ptr<gdapp::Connection> internal_connection;
	gdapp::GSharedPtr<GdaConnection> ptr_cnc_return;

	void SetUp() override {
	    cnc = std::make_shared<ConnectionMock>();
	    gdapp::SQLiteProvider provider{"DB_NAME=:memory:"};
	    internal_connection = std::make_shared<gdapp::SQLConnection>(provider);
	    internal_connection->open();

	    gdapp::SQLStatement stmt(internal_connection);
	    stmt.prepare("CREATE TABLE IF NOT EXISTS test_table (id integer)").execute();
	    ASSERT_TRUE(internal_connection->isOpen());
	    ptr_cnc_return = internal_connection->gobj();
	}
};

TEST(Statement, DefaultConstructor)
{
    std::shared_ptr<ConnectionMock> cnc = std::make_shared<ConnectionMock>();

    gdapp::SQLStatement stmt(cnc);
}

TEST_F(StatementTest, WithClosedConnection)
{
    //EXPECT_CALL(*cnc, gobj()).WillOnce(testing::Return(nullptr));
    EXPECT_CALL(*cnc, isOpen()).WillOnce(::testing::Return(false));

    gdapp::SQLStatement stmt(cnc);

    EXPECT_THROW(stmt.prepare("Random text"), gdapp::SQLexception);
}

TEST_F(StatementTest, WithOpenedConnectionWithUnknownPlaceHolderType)
{
    EXPECT_CALL(*cnc, gobj()).WillOnce(testing::Return(ptr_cnc_return));
    EXPECT_CALL(*cnc, isOpen()).WillOnce(testing::Return(true));

    gdapp::SQLStatement stmt(cnc);

    EXPECT_THROW(stmt.prepare("SELECT * FROM test_table WHERE id = ##one::unknwntype"), Glib::Error);
}

TEST_F(StatementTest, WithOpenedConnectionUsingCorrectPlaceHolderType)
{
    EXPECT_CALL(*cnc, gobj()).WillOnce(testing::Return(ptr_cnc_return));
    EXPECT_CALL(*cnc, isOpen()).WillOnce(testing::Return(true));

    gdapp::SQLStatement stmt(cnc);

    EXPECT_NO_THROW(stmt.prepare("SELECT * FROM test_table WHERE id = ##one::int"));
}

TEST_F(StatementTest, WithOpenedConnectionTwoCalls)
{
    EXPECT_CALL(*cnc, gobj()).Times(2).WillOnce(testing::Return(ptr_cnc_return)).WillOnce(testing::Return(ptr_cnc_return));
    EXPECT_CALL(*cnc, isOpen()).Times(2).WillOnce(testing::Return(true)).WillOnce(testing::Return(true));

    gdapp::SQLStatement stmt(cnc);

    EXPECT_NO_THROW(stmt.prepare("SELECT * FROM test_table WHERE id = ##one::int"));
    EXPECT_NO_THROW(stmt.prepare("SELECT * FROM test_table WHERE id = ##one::int"));
}

TEST_F(StatementTest, BindWithEmptyPlaceHolder)
{
    EXPECT_CALL(*cnc, gobj()).WillOnce(testing::Return(ptr_cnc_return));
    EXPECT_CALL(*cnc, isOpen()).WillOnce(testing::Return(true));

    gdapp::SQLStatement stmt(cnc);

    stmt.prepare("SELECT * FROM test_table WHERE id = ##one::int");
    EXPECT_THROW(stmt.bind("", 1), gdapp::SQLexception);
}

TEST_F(StatementTest, BindCorrectPlaceHolder)
{
    EXPECT_CALL(*cnc, gobj()).WillOnce(testing::Return(ptr_cnc_return));
    EXPECT_CALL(*cnc, isOpen()).WillOnce(testing::Return(true));

    gdapp::SQLStatement stmt(cnc);

    stmt.prepare("SELECT * FROM test_table WHERE id = ##one::int");
    EXPECT_NO_THROW(stmt.bind("one", 1));
}

TEST_F(StatementTest, BindCorrectPlaceHolderButWrongValue)
{
    EXPECT_CALL(*cnc, gobj()).WillOnce(testing::Return(ptr_cnc_return));
    EXPECT_CALL(*cnc, isOpen()).WillOnce(testing::Return(true));

    gdapp::SQLStatement stmt(cnc);

    stmt.prepare("SELECT * FROM test_table WHERE id = ##one::int");
    EXPECT_THROW(stmt.bind("one", gdapp::Variant("some_text")), Glib::Error);
}

TEST_F(StatementTest, BindNULLAblePlaceHolder)
{
    EXPECT_CALL(*cnc, gobj()).WillOnce(testing::Return(ptr_cnc_return));
    EXPECT_CALL(*cnc, isOpen()).WillOnce(testing::Return(true));

    gdapp::SQLStatement stmt(cnc);

    stmt.prepare("SELECT * FROM test_table WHERE id = ##one::int::NULL");
    EXPECT_THROW(stmt.bind("oneret", nullptr), gdapp::SQLexception);
}

TEST_F(StatementTest, BindNULLAblePlaceHolderWithCorrectPlaceHolder)
{
    EXPECT_CALL(*cnc, gobj()).WillOnce(testing::Return(ptr_cnc_return));
    EXPECT_CALL(*cnc, isOpen()).WillOnce(testing::Return(true));

    gdapp::SQLStatement stmt(cnc);

    stmt.prepare("SELECT * FROM test_table WHERE id = ##one::int::NULL");
    EXPECT_NO_THROW(stmt.bind("one", nullptr));
}

TEST_F(StatementTest, BindNULLToTheNotNullablePlaceholder)
{
    EXPECT_CALL(*cnc, gobj()).WillOnce(testing::Return(ptr_cnc_return));
    EXPECT_CALL(*cnc, isOpen()).WillOnce(testing::Return(true));

    gdapp::SQLStatement stmt(cnc);

    stmt.prepare("SELECT * FROM test_table WHERE id = ##one::int");
    EXPECT_THROW(stmt.bind("one", nullptr), Glib::Error);
}

TEST_F(StatementTest, RunningSimpleExecute)
{
    EXPECT_CALL(*cnc, gobj())
        .Times(2)
        .WillOnce(testing::Return(ptr_cnc_return))
        .WillOnce(testing::Return(ptr_cnc_return));
    EXPECT_CALL(*cnc, isOpen()).WillOnce(testing::Return(true));

    gdapp::SQLStatement stmt(cnc);

    stmt.prepare("INSERT INTO test_table (id) VALUES (1)");

    EXPECT_NO_THROW(stmt.execute());
}

TEST_F(StatementTest, RunningExecuteWithWrongStatement)
{
    EXPECT_CALL(*cnc, gobj())
	.Times(2)
	.WillOnce(testing::Return(ptr_cnc_return))
	.WillOnce(testing::Return(ptr_cnc_return));
    EXPECT_CALL(*cnc, isOpen()).WillOnce(testing::Return(true));

    gdapp::SQLStatement stmt(cnc);

    stmt.prepare("INSERT INTO random (id) VALUES (1)");

    EXPECT_THROW(stmt.execute(), Glib::Error);
}

TEST_F(StatementTest, CheckingLastRowAfterSelect)
{
    EXPECT_CALL(*cnc, gobj())
	.Times(2)
	.WillOnce(testing::Return(ptr_cnc_return))
	.WillOnce(testing::Return(ptr_cnc_return));
    EXPECT_CALL(*cnc, isOpen()).WillOnce(testing::Return(true));

    gdapp::SQLStatement stmt(cnc);

    stmt.prepare("INSERT INTO test_table (id) VALUES (1)");
    stmt.execute();

    auto row = stmt.last_row();
    int value;

    EXPECT_NO_THROW(value = row["id"].getInt());
    EXPECT_THAT(value, ::testing::Eq(1));
}

TEST_F(StatementTest, CheckingLastRowAfterSelectRunTwice)
{
    EXPECT_CALL(*cnc, gobj())
	.Times(4)
	.WillOnce(testing::Return(ptr_cnc_return))
	.WillOnce(testing::Return(ptr_cnc_return))
	.WillOnce(testing::Return(ptr_cnc_return))
	.WillOnce(testing::Return(ptr_cnc_return));
    EXPECT_CALL(*cnc, isOpen())
	.Times(2)
	.WillOnce(testing::Return(true))
	.WillOnce(testing::Return(true));

    gdapp::SQLStatement stmt(cnc);

    stmt.prepare("INSERT INTO test_table (id) VALUES (1)");
    stmt.execute();
    stmt.prepare("INSERT INTO test_table (id) VALUES (2)");
    stmt.execute();

    auto row = stmt.last_row();
    int value;

    EXPECT_NO_THROW(value = row["id"].getInt());
    EXPECT_THAT(value, ::testing::Eq(2));
}

TEST_F(StatementTest, CallingQueryBeforePrepare)
{
    EXPECT_CALL(*cnc, gobj())
	.Times(1)
	.WillOnce(testing::Return(ptr_cnc_return));

    gdapp::SQLStatement stmt(cnc);
    EXPECT_THROW(stmt.query(), Glib::Error);
}

TEST_F(StatementTest, CallingQueryWithWrongPrepare)
{
    EXPECT_CALL(*cnc, gobj())
	.Times(2)
	.WillOnce(testing::Return(ptr_cnc_return))
	.WillOnce(testing::Return(ptr_cnc_return));
    EXPECT_CALL(*cnc, isOpen())
	.Times(1)
	.WillOnce(testing::Return(true));

    gdapp::SQLStatement stmt(cnc);
    stmt.prepare("SELECT * FROM test_tablew WHERE id = 1");
    EXPECT_THROW(stmt.query(), Glib::Error);
}

TEST_F(StatementTest, CallingQueryWithCorrectPrepare)
{
    EXPECT_CALL(*cnc, gobj())
	.Times(2)
	.WillOnce(testing::Return(ptr_cnc_return))
	.WillOnce(testing::Return(ptr_cnc_return));
    EXPECT_CALL(*cnc, isOpen())
	.Times(1)
	.WillOnce(testing::Return(true));

    gdapp::SQLStatement stmt(cnc);
    stmt.prepare("SELECT * FROM test_table WHERE id = 1");
    EXPECT_NO_THROW(stmt.query());
}

int main(int argc, char *argv[]) {
    testing::InitGoogleMock(&argc, argv);
    return RUN_ALL_TESTS();
}

