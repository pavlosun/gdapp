#include <gtest/gtest.h>
#include "../gdapp-Connection.hpp"
#include "../gdapp-Statement.hpp"
#include "../gdapp-Table.hpp"

//MOCK_BASE_CLASS(ConnectionMock, gdapp::Connection) {
  //public:
    //MOCK_METHOD(open, 0, void(void));
    //MOCK_METHOD(close, 0, void(void));
    //MOCK_CONST_METHOD(isOpen, 0, bool(void));
    //MOCK_METHOD(gobj, 0, gdapp::GSharedPtr<GdaConnection>(void));
//};

class MyTable : public gdapp::BaseTable {
  public:
    inline static gdapp::Column<int> id{"id", gdapp::PrimaryKey(true), gdapp::AutoincrementKey(true)};
    inline static gdapp::Column<std::string> name{"name", gdapp::SizeKey(20)};
    inline static const std::string tableName = "pashatable";

    MyTable() {
        add(id);
        add(name);

        set_name(tableName);
    }
};

class RefTable: public gdapp::BaseTable {
  public:
    inline static gdapp::Column<int> id{"id", gdapp::PrimaryKey(true), gdapp::AutoincrementKey(true)};
    inline static gdapp::Column<int> mytable_id{"mytable_id"};
    inline static gdapp::ForeignKey fkey_mytable_id{"pashatable", "mytable_id", "id",
                                                    gdapp::OnDelete(gdapp::KeyReferenceActions::CASCADE)};
    inline static const std::string tableName = "fkeytable";

    RefTable() {
        add(id);
        add(mytable_id);
	add(fkey_mytable_id);

        set_name(tableName);
    }
};

TEST(Table, ctor)
{
    gdapp::SQLiteProvider provider{"DB_NAME=:memory:"};
    gdapp::SQLConnection cnc{provider};

    cnc.open();

    MyTable table;
    RefTable rtable;

    table.create(cnc);
    rtable.create(cnc);
}

int main(int argc, char *argv[]) {
    testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}

