#include "gmock/gmock-matchers.h"
#include <gtest/gtest.h>
#include "../gdapp-Connection.hpp"
#include "../gdapp-Exception.hpp"
#include <glibmm.h>
#include <map>

class ProviderTest : public ::testing::Test {
    public:
	gdapp::SQLiteProvider sqliteprovider{"sqlite_cnc"};
	gdapp::SQLiteProvider sqliteprovider_auth{"sqlite_cnc", "sqlite_auth"};
	gdapp::MySQLProvider mysqlprovider{"mysql_cnc", "mysql_auth"};
	gdapp::PostgreSQLProvider psqlprovider{"psql_cnc", "psql_auth"};
};

TEST(Provider, MemoryProviderWithDefaultAuth)
{
    gdapp::SQLiteProvider mem_provider{"mem_cnc"};
    EXPECT_EQ(mem_provider.get_cnc_string(), "SQLite://mem_cnc");
}

TEST(Provider, MemoryProviderWithAuth)
{
    gdapp::SQLiteProvider mem_provider{"mem_cnc", "auth_str"};
    EXPECT_EQ(mem_provider.get_auth_string(), "auth_str");
    EXPECT_EQ(mem_provider.get_cnc_string(), "SQLite://mem_cnc");
}

TEST(Provider, SQLiteProviderGetName)
{
    gdapp::SQLiteProvider mem_provider{"mem_cnc", "auth_str"};
    EXPECT_EQ(mem_provider.name(), "SQLite");
}

TEST(Provider, MySQLProviderGetName)
{
    gdapp::MySQLProvider mem_provider{"mem_cnc", "auth_str"};
    EXPECT_EQ(mem_provider.name(), "MySQL");
}

TEST(Provider, PostgreSQLProviderGetName)
{
    gdapp::PostgreSQLProvider mem_provider{"mem_cnc", "auth_str"};
    EXPECT_EQ(mem_provider.name(), "PostgreSQL");
}

TEST(Connection, WithCtorAuthDefault)
{
    gdapp::SQLiteProvider provider{"DB_NAME=:memory:"};
    EXPECT_NO_THROW(gdapp::SQLConnection cnc(provider));
}

TEST(Connection, CtorAuthProvided)
{
    gdapp::SQLiteProvider provider{"DB_NAME=:memory:", "nopaswd"};
    EXPECT_NO_THROW(gdapp::SQLConnection cnc(provider));
}

TEST(Connection, CallOpenWithWrongProvider)
{
    gdapp::PostgreSQLProvider provider{""};
    EXPECT_THROW(
        {
            gdapp::SQLConnection cnc(provider);
            cnc.open();
        },
        Glib::Error);
}

TEST(Connection, CallCloseBeforeOpen)
{
    gdapp::SQLiteProvider provider{"DB_NAME=:memory:"};

    EXPECT_NO_THROW(
        {
            gdapp::SQLConnection cnc(provider);
            cnc.close();
        });
}

TEST(Connection, CallIsOpenFalse)
{
    gdapp::SQLiteProvider provider{"DB_NAME=:memory:"};
    gdapp::SQLConnection cnc(provider);

    EXPECT_FALSE(cnc.isOpen());
}

TEST(Connection, CallOpen)
{
    gdapp::SQLiteProvider provider{"DB_NAME=:memory:"};
    gdapp::SQLConnection cnc(provider);

    EXPECT_NO_THROW(cnc.open());
    EXPECT_TRUE(cnc.isOpen());
}

TEST(Connection, TwoCallOpen)
{
    gdapp::SQLiteProvider provider{"DB_NAME=:memory:"};
    gdapp::SQLConnection cnc(provider);

    EXPECT_NO_THROW(cnc.open());
    EXPECT_NO_THROW(cnc.open());
}

TEST(Connection, CheckgobjReturnPointer)
{
    gdapp::SQLiteProvider provider{"DB_NAME=:memory:"};
    gdapp::SQLConnection cnc(provider);

    EXPECT_NO_THROW(cnc.open());
    EXPECT_THAT(cnc.gobj(), ::testing::Ne(nullptr));
}

static int open_event_count = 0;

void on_connection_open() {
    static std::mutex cnc_open_mutex;
    std::lock_guard<std::mutex> local_mutex{cnc_open_mutex};

    open_event_count++;
}

static int close_event_count = 0;

void on_connection_close() {
    static std::mutex cnc_close_mutex;
    std::lock_guard<std::mutex> local_mutex{cnc_close_mutex};

    close_event_count++;
}

TEST(Connection, SignalEmmited)
{
    gdapp::SQLiteProvider provider{"DB_NAME=:memory:"};
    gdapp::SQLConnection cnc(provider);

    cnc.signal_open().connect(sigc::ptr_fun(&on_connection_open));
    cnc.signal_close().connect(sigc::ptr_fun(&on_connection_close));
    EXPECT_NO_THROW(cnc.open());
    EXPECT_TRUE(cnc.isOpen());
    EXPECT_NO_THROW(cnc.close());

    EXPECT_EQ(open_event_count, 1);
    EXPECT_EQ(close_event_count, 1);
}

int main(int argc, char *argv[]) {
    testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}
