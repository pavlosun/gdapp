
#pragma once

#include <giomm.h>
#include <glibmm.h>
#include <libgda/libgda.h>
#include <utility>
#include "gdapp-Types.hpp"

namespace gdapp {

class Connection;

class Catalog {

    virtual void vparseFile(const std::string &) = 0;
    virtual void vparseFile(const Glib::RefPtr<Gio::File> &) = 0;
    virtual void vexecute() = 0;

    auto operator=(const Catalog &) -> Catalog & = default;

  public:
    virtual ~Catalog() = default;
    Catalog() = default;
    Catalog(const Catalog &) = default;
    Catalog(Catalog &&) noexcept = default;
    auto operator=(Catalog &&) noexcept -> Catalog & = default;

    void parseFile(const std::string &file) { vparseFile(file); }
    void parseFile(const Glib::RefPtr<Gio::File> &file) { vparseFile(file); }
    void execute() { vexecute(); }
};

class SQLCatalog final : public Catalog {

    gdapp::GPtr<GdaDbCatalog> _gobject;

  public:
    explicit SQLCatalog(gdapp::GSharedPtr<GdaConnection> &cnc)
        : _gobject{gdapp::GPtr<GdaDbCatalog>(gda_connection_create_db_catalog(cnc.get()))} {}

    void vparseFile(const std::string &dbfile) override {

        GFile *file = g_file_new_for_path(dbfile.c_str());

        GError *error = nullptr;
        bool res = gda_db_catalog_parse_file(_gobject.get(), file, &error);

        g_object_unref(file);

        if (!res)
            throw Glib::Error(error, true);
    }

    void vparseFile(const Glib::RefPtr<Gio::File> &file) override {
        GError *error = nullptr;
        bool res = gda_db_catalog_parse_file(_gobject.get(), file->gobj(), &error);

        if (!res)
            throw Glib::Error(error, true);
    }

    void vexecute() override {
        GError *error = nullptr;
        bool res = gda_db_catalog_perform_operation(_gobject.get(), &error);

        if (!res)
            throw Glib::Error(error, true);
    }
};
} // namespace gdapp
