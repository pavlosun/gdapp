#include "gdapp-Connection.hpp"
#include "gdapp-Exception.hpp"
#include <glibmm.h>

namespace gdapp {

SQLConnection::SQLConnection(const Provider &provider) : _pcnc{nullptr} {

    GError *error = nullptr;

    _pcnc = GSharedPtr<GdaConnection>(
        gda_connection_new_from_string(provider.name().c_str(), provider.get_cnc_string().c_str(),
                                       provider.get_auth_string().c_str(), GDA_CONNECTION_OPTIONS_NONE, &error));

    if (!_pcnc)
        throw Glib::Error(error, true);

    //g_signal_connect_swapped(_pcnc.get(), "opened", (GCallback)SQLConnection::on_connection_open, this);
    connection_id = g_signal_connect(_pcnc.get(), "status-changed", (GCallback)SQLConnection::on_connection_status_changed, this);
}

SQLConnection::~SQLConnection() {
    g_signal_handler_disconnect(_pcnc.get(), connection_id);
}

void SQLConnection::open() {
    GError *error = nullptr;

    if (!gda_connection_open(_pcnc.get(), &error))
        throw Glib::Error(error, true);

    // if (isOpen())
    //_open_signal.emit();
}

void SQLConnection::close() {
    if (!_pcnc || !isOpen())
        return;

    GError *error = nullptr;
    if (!gda_connection_close(_pcnc.get(), &error))
        throw Glib::Error(error, true);

    //_pcnc.reset();

    /*
     * Could be bug in libgda. signal is not emmited on close API call.
     *
     * */
    //if (!isOpen())
        //_close_signal.emit();
}

auto SQLConnection::isOpen() const -> bool {
    bool res{false};

    if (_pcnc)
        res = gda_connection_is_opened(_pcnc.get()) ? true : false;

    return res;
}

GSharedPtr<GdaConnection> SQLConnection::gobj() { return _pcnc; }

//void SQLConnection::on_connection_open(Connection *user_data) { user_data->_open_signal.emit(); }

//void SQLConnection::on_connection_close(Connection *user_data) { user_data->_close_signal.emit(); }
void SQLConnection::on_connection_status_changed([[maybe_unused]] GdaConnection *cnc,
                                                 GdaConnectionStatus status, gpointer user_data) {
    Connection *cppcon = reinterpret_cast<Connection *>(user_data);

    if (status == GDA_CONNECTION_STATUS_CLOSED)
	cppcon->_close_signal.emit();
    else if (status == GDA_CONNECTION_STATUS_OPENING)
	cppcon->_open_signal.emit();
}

} // namespace gdapp
