#pragma once

#include <string>
#include <libgda/libgda.h>

namespace gdapp {

enum class ColumnAttribute {
    PrimaryKey, // bool
    Autoinc, // bool
    Unique, // bool
    Nullable, // bool
    Size, // int
    Check, // string
    Comment, // string
    Default, // string
    Scale // int
};

template <ColumnAttribute attribute, class T>
class ColumnKey {
    const ColumnAttribute _attribute = attribute;
    T _value;

    public:
	explicit ColumnKey(T value) : _value(value) {}
	const ColumnAttribute &getAttribute() const noexcept { return _attribute;}
	const T &getValue() const noexcept { return _value;}
};

using PrimaryKey = ColumnKey<ColumnAttribute::PrimaryKey, bool>;
using AutoincrementKey = ColumnKey<ColumnAttribute::Autoinc, bool>;
using UniqueKey = ColumnKey<ColumnAttribute::Unique, bool>;
using NonNullKey = ColumnKey<ColumnAttribute::Nullable, bool>;
using SizeKey = ColumnKey<ColumnAttribute::Size, unsigned int>;
using ScaleKey = ColumnKey<ColumnAttribute::Scale, unsigned int>;
using CheckKey = ColumnKey<ColumnAttribute::Check, std::string>;
using CommentKey = ColumnKey<ColumnAttribute::Comment, std::string>;
using DefaultKey = ColumnKey<ColumnAttribute::Default, std::string>;


class BaseColumn {
  public:
    virtual GdaDbColumn *gobj() = 0;
    virtual const gchar *name() const = 0;
    virtual ~BaseColumn() = default;
};

template <typename T>
class TypePolicy {
    public:
        static GType get_g_type() {
            if (typeid(T) == typeid(int))
                return G_TYPE_INT;
            else if (typeid(T) == typeid(unsigned int))
                return G_TYPE_UINT;
            else if (typeid(T) == typeid(long))
                return G_TYPE_LONG;
            else if (typeid(T) == typeid(unsigned long))
                return G_TYPE_ULONG;
            else if (typeid(T) == typeid(double))
                return G_TYPE_DOUBLE;
            else if (typeid(T) == typeid(float))
                return G_TYPE_FLOAT;
            else if (typeid(T) == typeid(std::string))
                return G_TYPE_STRING;
            else
                return GDA_TYPE_NULL;
        }
};

template<
class T,
template <class> class TPolicy = TypePolicy
>
class Column : public TPolicy<T>, public BaseColumn {
    GdaDbColumn *_column = nullptr;

    template<class ... Ts>
    Column(PrimaryKey key, Ts... args): Column(args...) {
	gda_db_column_set_pkey(_column,  key.getValue() ? TRUE : FALSE);
    }

    template<class ... Ts>
    Column(AutoincrementKey key, Ts... args): Column(args...) {
	gda_db_column_set_autoinc(_column, key.getValue() ? TRUE : FALSE);
    }

    template<class ... Ts>
    Column(UniqueKey key, Ts... args): Column(args...) {
	gda_db_column_set_unique(_column, key.getValue() ? TRUE : FALSE);
    }

    template<class ... Ts>
    Column(NonNullKey key, Ts... args): Column(args...) {
	gda_db_column_set_nnul(_column, key.getValue() ? TRUE : FALSE);
    }

    template<class ... Ts>
    Column(SizeKey key, Ts... args): Column(args...) {
	gda_db_column_set_size(_column, key.getValue());
    }

    template<class ... Ts>
    Column(ScaleKey key, Ts... args): Column(args...) {
	gda_db_column_set_scale(_column, key.getValue());
    }

    template<class ... Ts>
    Column(CheckKey key, Ts... args): Column(args...) {
	gda_db_column_set_check(_column, key.getValue().c_str());
    }

    template<class ... Ts>
    Column(CommentKey key, Ts... args): Column(args...) {
	gda_db_column_set_comment(_column, key.getValue().c_str());
    }

    template<class ... Ts>
    Column(DefaultKey key, Ts... args): Column(args...) {
	gda_db_column_set_default(_column, key.getValue().c_str());
    }

    Column() {
        if (!_column) {
            _column = gda_db_column_new();
        }
    }

   public:
    template <class... Ts> Column(const char *name, Ts... args) : Column(args...) {
        gda_db_column_set_name(_column, name);
        gda_db_column_set_type(_column, TypePolicy<T>::get_g_type());
    }

    const gchar *name() const override { return gda_db_column_get_name(_column); }
    GdaDbColumn *gobj() override { return _column; }

    virtual ~Column() { g_object_unref(_column); }
};

}
