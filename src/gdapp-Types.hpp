
#pragma once

#include <functional>
#include <memory>
#include <libgda.h>

namespace gdapp {

using GValueDeleter = std::function<void(GValue *)>;
using GValuePtr = std::unique_ptr<GValue, GValueDeleter>;

using GObjectDeleter = std::function<void(void *)>;

template <typename T> class GPtr : public std::unique_ptr<T, GObjectDeleter> {
  public:
    explicit GPtr(T *obj)
        : std::unique_ptr<T, GObjectDeleter>(obj, [](void *obj) {
              if (obj)
                  g_object_unref(obj);
          }) {}
};

template <typename T> class GSharedPtr : public std::shared_ptr<T> {
  public:
    GSharedPtr() : std::shared_ptr<T>() {}

    explicit GSharedPtr(T *obj)
        : std::shared_ptr<T>(obj, [](void *obj) {
              if (obj)
                  g_object_unref(obj);
          }) {}
};
} // namespace gdapp
