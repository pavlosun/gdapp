#pragma once

#include <glibmm.h>
#include <libgda.h>
#include "gdapp-Types.hpp"
#include "gdapp-Exception.hpp"
#include "gdapp-Variant.hpp"

namespace gdapp {

class DataModel {
    virtual auto vNext() -> bool = 0;
    virtual auto vValue(const std::string_view &str) -> gdapp::Variant = 0;

    auto operator=(const DataModel &) -> DataModel & = default;

  public:
    auto next() -> bool { return vNext(); }
    auto Value(const std::string_view &str) -> gdapp::Variant { return vValue(str); }

    DataModel() = default;
    DataModel(const DataModel &) = default;
    DataModel(DataModel &&) noexcept = default;
    auto operator=(DataModel &&) noexcept -> DataModel & = default;
    virtual ~DataModel() = default;
};

class SQLDataModel final : public DataModel {
    gdapp::GPtr<GdaDataModel> _dm;
    gdapp::GPtr<GdaDataModelIter> _it;

    auto vNext() -> bool override { return gda_data_model_iter_move_next(_it.get()); }

    auto vValue(const std::string_view &str) -> gdapp::Variant override {
        auto cGval = gda_data_model_iter_get_value_for_field(_it.get(), str.data());

        if (!cGval)
            throw gdapp::SQLexception("Can't get int type for column ", __FILE__, __LINE__);

        return gdapp::Variant::create(cGval);
    }

  public:
    explicit SQLDataModel(GdaDataModel *obj) : _dm{obj}, _it{gda_data_model_create_iter(obj)} {}
};

} // namespace gdapp
