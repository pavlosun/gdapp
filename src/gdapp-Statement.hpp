#pragma once

#include <glibmm.h>
#include <libgda/libgda.h>
#include "gdapp-Exception.hpp"
#include "gdapp-Types.hpp"
#include "gdapp-DataModel.hpp"
#include "gdapp-Connection.hpp"
#include <cstddef>

namespace gdapp {

class Statement {
  public:
    virtual auto bind(const std::string &id, gdapp::Variant) -> Statement & = 0;
    virtual auto bind(const std::string &id, std::nullptr_t val) -> Statement & = 0;
    virtual auto prepare(const std::string &sql) -> Statement & = 0;
    virtual void execute() = 0;
    virtual auto query() -> std::unique_ptr<gdapp::DataModel> = 0;
    [[nodiscard]] virtual auto last_row() const -> const std::map<std::string, gdapp::Variant> & = 0;

    Statement() = default;
    Statement(const Statement &) = default;
    auto operator=(const Statement &) -> Statement & = default;
    Statement(Statement &&) noexcept = default;
    auto operator=(Statement &&) noexcept -> Statement & = default;

    virtual ~Statement() = default;
};

class SQLStatement : public Statement {
    GPtr<GdaStatement> _stmt;
    GPtr<GdaSet> _pset;
    std::shared_ptr<gdapp::Connection> ptrcnc;
    std::map<std::string, gdapp::Variant> _last_row;

  public:
    SQLStatement(std::shared_ptr<gdapp::Connection>);
    auto bind(const std::string &id, gdapp::Variant val) -> Statement & override;
    auto bind(const std::string &id, [[maybe_unused]] std::nullptr_t val) -> Statement & override;
    auto prepare(const std::string &sql) -> Statement & override;
    void execute() override;
    auto query() -> std::unique_ptr<gdapp::DataModel> override;
    [[nodiscard]] auto last_row() const -> const std::map<std::string, gdapp::Variant> & override;
};

} // namespace gdapp
